//jQuery wrapper
(function ($) {
  //Define a Drupal behaviour with a custom name
  Drupal.behaviors.webform_validation_no_ccsAddNoCcs = {
    attach: function (context) {
      //Add an eventlistener to the document reacting on the
      //'clientsideValidationAddCustomRules' event.
      $(document).bind('clientsideValidationAddCustomRules', function(event){
        //Add your custom method with the 'addMethod' function of jQuery.validator
        //http://docs.jquery.com/Plugins/Validation/Validator/addMethod#namemethodmessage
        jQuery.validator.addMethod("noCcs", function(value, element, param) {
          //return true if valid, false if invalid
          //return !(valid_credit_card(value));
          var returnBoolean = true;
          var val = '';


          value = value.replace(/\D/g,'');


          
          while(value.length >= 15) {

            if (value.length >= 16) {
              val = value.substr(0, 16);

              if (valid_credit_card(val)) {
                returnBoolean = false;
              }
            }
            val = value.substr(0, 15);

            if (valid_credit_card(val)) {
              returnBoolean = false;
            }
            value = value.substr(1);
          }
          return returnBoolean;
          
          //Enter a default error message.
        }, jQuery.format('Value must a valid phone number'));
      });
    }
  }
  
  function valid_credit_card(value) {
  // accept only digits, dashes or spaces
	if (/[^0-9-\s]+/.test(value)) return false;
 
	// The Luhn Algorithm. It's so pretty.
	var nCheck = 0, nDigit = 0, bEven = false;
	value = value.replace(/\D/g, "");
 
	for (var n = value.length - 1; n >= 0; n--) {
		var cDigit = value.charAt(n),
			  nDigit = parseInt(cDigit, 10);
 
		if (bEven) {
			if ((nDigit *= 2) > 9) nDigit -= 9;
		}
 
		nCheck += nDigit;
		bEven = !bEven;
	}
 
	return (nCheck % 10) == 0;
}

  
  
})(jQuery);